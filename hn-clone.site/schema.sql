create database if not exists hnapi;
use hnapi;

create table if not exists `post` (
  `id` INT PRIMARY KEY,
  `url` VARCHAR(500),
  `title` VARCHAR(90),
  `commentCount` INT,
  `points` INT,
  `postedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table if not exists `user` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `username` VARCHAR(128) NOT NULL UNIQUE KEY,
  `pass` VARCHAR(200) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table if not exists `tag` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `user` INT NOT NULL,
  `post` INT NOT NULL,
  `type` VARCHAR(64) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY (`user`, `post`, `type`)
);