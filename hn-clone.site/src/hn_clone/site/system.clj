(ns hn-clone.site.system
  (:require [hn-clone.protocols.db :as db]
            [hn-clone.site.scrape :as scrape]
            [duct.database.sql :as sql]
            [clojure.java.jdbc :as jdbc]
            [honeysql.core :as hon]
            [honeysql.helpers :as honey]
            [buddy.hashers :as hashers]
            [integrant.core :as ig]
            [java-time :as t]))

(extend-type duct.database.sql.Boundary
  db/DB
    (upsert-posts [this posts]
      (jdbc/with-db-connection [conn (:spec this)]
        (-> (honey/insert-into :post)
            (honey/values (mapv (fn [post]
                                  (update post
                                          :postedOn
                                          #(t/sql-timestamp (t/local-date-time % (t/zone-id))))) posts))
            hon/format
            (update 0 str " on duplicate key update commentCount=values(commentCount), points=values(points)")
            (->> (jdbc/execute! conn)))))
    (create-user [this username pass]
      (jdbc/with-db-connection [conn (:spec this)]
        (try
          (let [user-id (->> {:username username
                              :pass (hashers/derive pass {:alg :pbkdf2+sha512})}
                             (jdbc/insert! conn :user)
                             first
                             :generated_key)]
            {:id user-id})
          (catch Exception e
            {:error (if (instance? java.sql.SQLIntegrityConstraintViolationException e)
                        :duplicate
                        :unknown)}))))
    (authenticate-user [this username password]
      (jdbc/with-db-connection [conn (:spec this)]
        (let [[{enc :pass}] (jdbc/query conn ["select pass from user where username = ?" username])]
          (and (hashers/check password enc) {:username username}))))
    (get-posts [this username]
      (jdbc/with-db-connection [conn (:spec this)]
        (jdbc/query conn ["select p.*, t.type as tag from post p left join (select t.* from tag t inner join user u on u.id = t.user where u.username = ?) t on t.post = p.id where t.type is null or t.type != 'hidden' order by p.postedOn desc limit 90" username])))
    (tag-posts [this username taggings]
      (jdbc/with-db-connection [conn (:spec this)]
        (let [[{uid :id}] (jdbc/query conn ["select id from user where username = ?" username])
              {adds true dels false} (group-by :op (mapv #(assoc % :user uid) taggings))]
          (when (not (empty? dels))
            (jdbc/execute! conn [(str "delete from tag where user = ? and (" (clojure.string/join " or " (repeat (count dels) "(post = ? and type = ?)")) ")") (flatten [uid (mapv (fn [{:keys [post type]}] [post type]) dels)])]))
          (when (not (empty? adds))
            (-> (honey/insert-into :tag)
                (honey/values (mapv #(select-keys % [:post :type :user]) adds))
                hon/format
                (update 0 str " on duplicate key update id=id")
                (->> (jdbc/execute! conn))))
          "done"))))

(defmethod ig/init-key ::update-db
  [_ {:keys [db batch-size]}]
  #(db/upsert-posts db (scrape/top-n-posts batch-size)))