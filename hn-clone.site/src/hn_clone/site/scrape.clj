(ns hn-clone.site.scrape
  (:require [net.cgrand.enlive-html :as html]
            [java-time :as t]))

(def base-url "https://news.ycombinator.com/news")

(defn- resource->posts
  "Turns an enlive resource of a HN page to a seq of posts"
  [resource]
  (letfn [(comment-node? [{tag :tag [txt] :content}]
            (and (= tag :a) (string? txt) (re-find #"comments" txt)))
          (extract-int [r] (->> r first :content first (#(or % "0")) (re-find #"\d+") Integer/parseInt))
          (item-to-post [{{id :id} :attrs :as athing} subtext]
            (let [[{{url :href} :attrs
                    [title] :content}] (html/select athing [:td.title :a])
                  [_ magnitude unit]   (->> (html/select subtext [:span.age :a])
                                            first :content first (re-find #"(\d+)\s(minute|hour|day)"))]
              {:url url :title title
               :id (Integer/parseInt id)
               :points (->> (html/select subtext [:span.score]) extract-int)
               :commentCount (->> subtext :content (filter comment-node?) extract-int)
               :postedOn (t/minus (t/instant)
                                  (t/duration (Integer/parseInt magnitude)
                                              (keyword (str unit "s"))))}))]
    (mapv item-to-post
          (html/select resource [:table.itemlist :tr.athing])
          (html/select resource [:table.itemlist :td.subtext]))))

(defn top-n-posts
  "Scrapes top n posts from hacker news"
  [n]
  (->> (drop 1 (range))
       (map #(html/html-resource (java.net.URI. (str base-url "?p=" %))))
       (mapcat resource->posts)
       (take n)))