(ns hn-clone.site.routes
  (:require
    [yada.yada :as yada]
    [integrant.core :as ig]
    [hiccup.page :refer [html5]]
    [buddy.sign.jwt :as jwt]
    [clojure.edn :as edn]
    [hn-clone.protocols.db :as db]
    [schema.core :as schema]
    [java-time :as t]
    yada.bidi
    yada.security
    yada.cookies))

(def default-responses
  {401 {:produces "text/html"
        :response (html5 [:h2 "You are not logged in"]
                         [:p "Click " [:a {:href "/login"} "here"] " to go to the login page."])}})

(defn- inst-to-str [ts]
  (let [tsn (t/instant)
        f #(t/as (t/duration (t/instant ts) tsn) %)
        [n unit] (->> [:days :hours :minutes]
                      (map #(vector (f %) %))
                      (filter #(pos? (first %)))
                      first
                      (#(or % [0 :minutes])))
        s (str n " " (name unit))]
    (if (= n 1)
      (subs s 0 (dec (count s)))
      s)))

(defn- render-post
  [idx {:keys [id title url commentcount points postedon]}]
  (list
   [:tr.athing {:id id}
    [:td.title {:align "right" :valign "top"} [:span.rank (str (inc idx) ".")]]
    [:td.title
     [:a.storylink {:href url :style "padding-left:5px"} title]
     [:span.sitebit.comhead
      " ("
      [:span.sitestr (or (second (re-find #"https?://(?:www\.)?([^/]+)" url)) "")]
      ")"]]]
   [:tr
    [:td {:colspan "1"}]
    [:td.subtext
     (interpose " | "
      [[:span.score {:id (str "score_" id)} (str points " points")]
       [:span.age (str (inst-to-str postedon) " ago")]
       [:a.link {:onclick (str "hide(" id ")")} "hide"]
       [:a {:href (str "https://news.ycombinator.com/item?id=" id)} (str commentcount " comments")]])]]))

(defn- header [{:keys [username]}]
  [:tr
   [:td {:bgcolor "#ff6600"}
    [:table {:border "0" :cellpadding "0" :cellspacing "0" :width "100%" :style "padding:2px"}
     [:tr
      [:td {:style "width:18px;padding-right:4px"}
       [:img {:src "data:image/gif;base64,R0lGODlhAQABAIAAAP"
              :width "18" :height "18" :style "border:1px white solid;"}]]
      [:td {:style "line-height:12pt; height:10px;"}
       [:span.pagetop
        [:b.hnname
         [:a {:href "news"} "Faker News"]]]]
      [:td {:style "text-align:right;padding-right:4px;"}
       [:span.pagetop
        [:form#logoutForm {:action "/logout" :method :post}]
        [:a#me username]
        " | "
        [:a#logout.link {:onclick "logout()"} "logout"]]]]]]])

(defn- render-news [username posts]
  (list
   [:head
    [:meta {:name "referrer" :content "origin"}]
    [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
    [:link {:rel "stylesheet" :type "text/css" :href "/static/site.css"}]
    [:title "Faker News"]]
   [:body
    [:center
     [:table#hnmain {:border "0" :cellpadding "0" :cellspacing "0" :width "85%" :bgcolor "#f6f6ef"}
      (header {:username username})
      [:tr#pagespace {:title "" :style "height:10px"}]
      [:tr
       [:td
        [:table.itemlist {:border "0" :cellpadding "0" :cellspacing "0"}
         (interpose [:tr.spacer {:style "height:5px"}]
           (map-indexed render-post posts))]]]]]
    [:script {:type "text/javascript" :src "/static/script.js"}]]))

(defn news [{:keys [db]}]
  (yada/resource
    {:id ::news
     :responses default-responses
     :methods
     {:get
      {:produces {:media-type "text/html" :charset "UTF-8"}
       :response
       (fn [ctx]
        (let [username (get-in ctx [:authentication "default" :username])
              posts (db/get-posts db username)]
          (html5 (render-news username posts))))}}
     :access-control
     {:scheme ::signed-cookie
      :authorization {:methods {:get :user}}}}))

(def login-form
  (html5
   [:b "Login"
    [:form {:method :post}
     [:p
      [:label {:for "user"} "username: "]
      [:input {:type :text :name "user"}]]
     [:p
      [:label {:for "password"} "password: "]
      [:input {:type :password :name "pass"}]]
     [:p
      [:input {:type :submit :value "login"}]]]
    [:b "Create Account"]
    [:form {:method :post}
     [:p
      [:label {:for "user"} "username: "]
      [:input {:type :text :name "user"}]]
     [:p
      [:label {:for "password"} "password: "]
      [:input {:type :password :name "pass"}]
      [:input {:type :hidden :id "create" :name "create" :value "true"}]]
     [:p
      [:input {:type :submit :value "create account"}]]]]))

(defn sign-in-or-sign-up-handler [ctx {:keys [db secret]}]
  (let [{:keys [user pass create]} (-> ctx :parameters :form)
        goto (-> ctx :parameters :query :goto (or "news"))]
    (merge
     (:response ctx)
     (let [signed-cookie (jwt/sign
                          {:claims (pr-str {:username user :roles #{:user}})}
                          secret)
           succ {:status 303
                 :headers {"location" (str "/" goto)}
                 :cookies
                 {"session" {:max-age 3600
                             :domain "localhost"
                             :value signed-cookie}}}]
       (cond (and (not create) (db/authenticate-user db user pass)) succ
             (not create) {:status 401 :body "Login failed"}
             :else
               (let [{:keys [id error]} (db/create-user db user pass)]
                 (if id succ {:status 401 :body (if (= :duplicate error) "Duplicate username" "account creation failed")})))))))

(defn login [cfg]
  (yada/resource
   {:id ::login
    :methods
    {:get
     {:produces "text/html"
      :response login-form}
     :post
     {:consumes "application/x-www-form-urlencoded"
      :produces "text/plain"
      :parameters
      {:form {:user String
              :pass String
              (schema/optional-key :create) String}
       :query {(schema/optional-key :goto) String}}
      :response #(sign-in-or-sign-up-handler % cfg)}}}))

(defn logout []
  (yada/resource
   {:id ::logout
    :methods
    {:post
     {:consumes "application/x-www-form-urlencoded"
      :produces "text/plain"
      :parameters {}
      :response
      (fn [ctx]
        (merge (:response ctx)
               {:status 303
                :headers {"location" "/login"}
                :cookies
                {"session" {:value "fake"
                            :expires (java.util.Date/from (t/minus (t/instant) (t/days 1)))}}}))}}}))

(defn tag [{:keys [db]}]
  (yada/resource
   {:id ::tag
    :methods
    {:post
     {:consumes "application/json"
      :produces "text/plain"
      :parameters
      {:body {:post Integer
              :tag String}}
      :response
      (fn [ctx]
        (let [{:keys [post tag]} (-> ctx :parameters :body)
              username (get-in ctx [:authentication "default" :username])]
          (db/tag-posts db username [{:post post :type tag :op true}])
          {:status 302 :headers {"location" "/news"}}))}}
    :access-control
     {:scheme ::signed-cookie
      :authorization {:methods {:post :user}}}}))


(defmethod ig/init-key ::routes
  [_ {:keys [db secret] :as cfg}]
  (defmethod yada.security/verify ::signed-cookie
    [ctx scheme]
    (some->
      (get-in ctx [:cookies "session"])
      (jwt/unsign secret)
      :claims
      edn/read-string))
  [["http://localhost:3000"
    ["/news" (news cfg)]
    ["/login" (login cfg)]
    ["/logout" (logout)]
    ["/tag" (tag cfg)]
    ["/static/"
      (yada.yada/as-resource
        (clojure.java.io/file (str (System/getProperty "user.dir") "/src/public/")))]]])



