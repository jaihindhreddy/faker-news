(ns hn-clone.protocols.db)

(defprotocol DB
  (upsert-posts [this posts])
  (create-user [this username pass])
  (authenticate-user [this username password]
    "must return falsy value if invalid user, otherwise return role information")
  (get-posts [this username]
    "get the top 90 posts in reverse chronological order with tag information (except delete tag)")
  (tag-posts [this username taggings]
    "tag posts. a tagging is a map with :post :type :op indicating post id, tag type and assertion"))