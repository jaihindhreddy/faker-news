function $(id) { return document.getElementById(id); }
function remEl(el) { el.parentNode.removeChild(el); }

function fixNums() {
	Array.prototype.forEach.call(
		document.getElementsByClassName('rank'),
		(rk, i) => { rk.innerHTML = (i+1)+'.'; }
	);
}

function hide(id) {
	fetch('/tag', {
		method: 'post',
		headers: {'Content-Type': 'application/json'},
		body: JSON.stringify({post: id, tag: 'hidden'})
	});
	remEl($(id).nextSibling);
	remEl($(id).nextSibling);
	remEl($(id));
	let rks = document.getElementsByClassName('rank');
	Array.prototype.forEach.call(document.getElementsByClassName('rank'), (rk, i) => { rk.innerHTML = (i+1)+'.'; });
};

function logout() { return document.querySelector('#logoutForm').submit(); }